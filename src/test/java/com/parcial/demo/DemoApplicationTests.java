package com.parcial.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Assertions;


@SpringBootTest
class DemoApplicationTests {
    
        static String [] arry1 = new String[]{"hello", "there", "life", "sucks"};
        static String [] arry2 = new String[]{"hello", "there", "life", "sucks"};
        
        static boolean isValidBullshit = true;
        static boolean lifeSucks = false;
        
        static String nullString = null;
        static String notnullString = "hello there";
        
	@Test
	void contextLoads() {
	}
        
        @Test
        void validateArray1equalsToArra2(){
            Assertions.assertArrayEquals(arry1, arry2);
        }
        
        @Test
        void validIfThisLifeIsTrue(){
            Assertions.assertTrue(isValidBullshit);
        }
        
        @Test
        void validIfLifeSucks(){
            Assertions.assertFalse(lifeSucks);
        }
        
        @Test
        void validIfStringIsNull(){
            Assertions.assertNotNull(notnullString, "hello there");
        }
        
        @Test
        void validaIfGarbageAreTheSame(){
            Assertions.assertNotSame(arry1, arry2);
        }
        
    
        
        

}
